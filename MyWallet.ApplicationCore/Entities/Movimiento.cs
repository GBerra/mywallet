﻿using System;

namespace MyWallet.ApplicationCore.Entities
{
    public class Movimiento: BaseEntity
    {
        public int IdConcepto { get; set; }
        public Concepto Concepto { get; set; }
    }
}
