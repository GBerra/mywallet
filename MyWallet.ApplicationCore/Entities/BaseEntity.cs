using System;

namespace MyWallet.ApplicationCore.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public DateTime Deleted { get; set; }

    }
}