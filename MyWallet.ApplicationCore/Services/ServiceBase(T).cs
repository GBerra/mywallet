using mywallet.applicationcore.Interfaces;
using MyWallet.ApplicationCore.Entities;

namespace mywallet.applicationcore.Services
{
    public class ServiceBase<T>: IServiceBase<T> where T : BaseEntity
    {
        private IRepositoryBase<T> _repository;

        private IUnitOfWork _unitOfWork;

        public ServiceBase(IRepositoryBase<T> repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public void Create(T entity)
        {
            _repository.Create(entity);
            _unitOfWork.SaveChanges();
        }

        public void Delete(T entity)
        {
            _repository.Delete(entity);
            _unitOfWork.SaveChanges();
        }

        public T[] Retrieve()
        {
            return _repository.Retrieve();           
        }

        public void Update(T entity)
        {
            _repository.Update(entity);
            _unitOfWork.SaveChanges();
        }
    }
}