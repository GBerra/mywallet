using MyWallet.ApplicationCore.Entities;

namespace mywallet.applicationcore.Interfaces
{
    public interface IUnitOfWork
    {
         void SaveChanges();
    }
}