using MyWallet.ApplicationCore.Entities;

namespace mywallet.applicationcore.Interfaces
{
    public interface IRepositoryBase<T> where T: BaseEntity
    {
         void Create(T entity);

         T[] Retrieve();

         void Update(T entity); 

         void Delete(T entity);
    }
}